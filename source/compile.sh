#
# Compile Java code for the Sudoku
# puzzle maker
#
# Since this program creates PDF output
# files, the FOP and associated JAR files
# must be included during compilation and
# execution.
#
# Since the program reads its configuations
# from JSON files, we are using the BFO JSON
# package.
#
JAV=javac

#LINT=
##LINT=" -Xlint:deprecation -Xlint:unchecked "
LINT=" -Xlint:deprecation  "


# following for FOP code
MAIN=build/fop.jar

# following for FOP code
CP=$CP:$MAIN
CP=$CP:lib/avalon-framework-api-4.3.1.jar
CP=$CP:lib/avalon-framework-impl-4.3.1.jar
CP=$CP:lib/batik-all-1.9.jar
CP=$CP:lib/commons-io-1.3.1.jar
CP=$CP:lib/commons-logging-1.0.4.jar
CP=$CP:lib/fontbox-2.0.4.jar
CP=$CP:lib/serializer-2.7.2.jar
CP=$CP:lib/xalan-2.7.2.jar
CP=$CP:lib/xercesImpl-2.9.1.jar
CP=$CP:lib/xml-apis-1.3.04.jar
CP=$CP:lib/xml-apis-ext-1.3.04.jar
CP=$CP:lib/xmlgraphics-commons-2.2.jar

# following for JSON
CP=$CP:bfojson-1.jar

# following for sudoku code
CP=$CP:commons-math3-3.4.1.jar
CP=$CP:.

##OBJ=test
OBJ=Sud

rm -v *.class

echo $JAV $LINT -classpath $CP $OBJ.java
$JAV $LINT -classpath $CP $OBJ.java

jar --create --file sud.jar *.class

