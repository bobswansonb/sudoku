**Swanson Sudoku Creator**


---

## Introduction

I enjoy working "sudoku" type puzzles. I became tired of the commercial products, and wondered if I could program my own. My
Java skills are OK, but figuring out many difficult algorithms became a barrier to any work on the project.

The good news is that the "Math with Bad Drawings" blog came up with a simpler scheme that allows
the creation of trillions of possible puzzles from a simple set of existing solutions. That
is the implementation you see here.

The input configuration files are JSON, which requires Java code to read it. After much work
with other systems the BFO JSON processor is now the tool of choice.

This program can create at a large number of levels, including simply supplying
a frequency graph of desired "givens", or a count of desired "givens".

## Dependencies

This Java code is dependent on:

FOP -- to create PDF file output of the puzzle

Math JAR -- for some statistical information about the puzzle

Batik -- lots of JAR's for creation of the PDF output

BFO JSON -- for reading the JSON configuration files
